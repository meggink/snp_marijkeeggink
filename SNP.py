#!usr/bin/env python3
"""
This program adds a SNP to a gene sequence and compares this to a MSA
"""

__author__ = "Marijke Eggink"
__version__ = "2020.v.1"
__date__ = "29-10-2020"

from Bio import AlignIO
import random
import sys
import argparse


class SNP:
    """
    This class adds a SNP to a gene sequence and compares it to a MSA and gives a score of the alignment
    """
    def __init__(self, index, nucleotide, sequence, msa):
        self.sequence = AlignIO.read(sequence, format="fasta")
        self.msa = AlignIO.read(msa, format="fasta")
        self.nuc_seq = []
        self.gaps = []
        self.index = index
        self.nucleotide = nucleotide
        self.aa_seq = ""
        self.pos = 0
        self.aa_to_nuc = {"A": ["GCG", "GCA", "GCC", "GCT"],
                          "C": ["TGT", "TGC"],
                          "D": ["GAC", "GAT"],
                          "E": ["GAG", "GAA"],
                          "F": ["TTT", "TTC"],
                          "G": ["GGG", "GGA", "GGC", "GGT"],
                          "H": ["CAT", "CAC"],
                          "I": ["ATT", "ATC", "ATA"],
                          "K": ["AAA", "AAG"],
                          "L": ["CTT", "CTC", "CTA", "CTG", "TTA", "TTG"],
                          "M": ["ATG"],
                          "N": ["AAT", "AAC"],
                          "P": ["CCT", "CCC", "CCA", "CCG"],
                          "Q": ["CAA", "CAG"],
                          "R": ["CGT", "CGC", "CGA", "CGG", "AGA", "AGG"],
                          "S": ["TCT", "TCC", "TCA", "TCG", "AGT", "AGC"],
                          "T": ["ACT", "ACC", "ACA", "ACG"],
                          "V": ["GTG", "GTA", "GTC", "GTT"],
                          "W": ["TGG"],
                          "Y": ["TAT", "TAC"],
                          "*": ["TAA", "TAG", "TGA"]}

        self.nuc_to_aa = {"TTT": "F", "TTC": "F", "TTA": "L", "TTG": "L",
                          "TCT": "S", "TCC": "S", "TCA": "S", "TCG": "S",
                          "TAT": "Y", "TAC": "Y", "TAA": " ", "TAG": " ",
                          "TGT": "C", "TGC": "C", "TGA": " ", "TGG": "W",
                          "CTT": "L", "CTC": "L", "CTA": "L", "CTG": "L",
                          "CCT": "P", "CCC": "P", "CCA": "P", "CCG": "P",
                          "CAT": "H", "CAC": "H", "CAA": "Q", "CAG": "Q",
                          "CGT": "R", "CGC": "R", "CGA": "R", "CGG": "R",
                          "ATT": "I", "ATC": "I", "ATA": "I", "ATG": "M",
                          "ACT": "T", "ACC": "T", "ACA": "T", "ACG": "T",
                          "AAT": "N", "AAC": "N", "AAA": "K", "AAG": "K",
                          "AGT": "S", "AGC": "S", "AGA": "R", "AGG": "R",
                          "GTT": "V", "GTC": "V", "GTA": "V", "GTG": "V",
                          "GCT": "A", "GCC": "A", "GCA": "A", "GCG": "A",
                          "GAT": "D", "GAC": "D", "GAA": "E", "GAG": "E",
                          "GGT": "G", "GGC": "G", "GGA": "G", "GGG": "G"}

    def index_alignment(self):
        """
        This method writes the sequence to a DNA sequence or gives the index of the gaps
        :return:
        """
        # Loop through the sequence
        for count, aa in enumerate(self.sequence[0, :]):
            # writes the amino acid to the a random three lettered DNA seq that belongs to the amino acid
            if aa in self.aa_to_nuc:
                self.nuc_seq += random.choice(self.aa_to_nuc[aa])
            else:
                # remember the index of the gaps
                self.gaps.append(count)

    def add_snp(self, index, nucleotide):
        """
        This method adds the SNP at the right index.
        :param index: Position where the SNP needs to be.
        :param nucleotide: The nucleotide that the SNP needs to be.
        :return:
        """
        # check if index is in range of the sequence
        if index in range(0, len(self.nuc_seq)):
            # if nucleotide is not a gap change the nucleotide
            if self.nucleotide != "-":
                self.nuc_seq[index] = nucleotide
            # if nucleotide is a gap than remove nucleotide
            else:
                self.nuc_seq[index] = ""
        # Positon is out of range
        else:
            print("Position is out of range. Try a number between 0 and", len(self.nuc_seq))
            sys.exit()

    def check_for_deletion(self):
        """
        This method checks if there is a deletion.
        :return:
        """
        self.nuc_seq = self.aa_seq.join(self.nuc_seq)
        # Checks if there is no deletion
        if len(self.sequence[0, :]) % 3 == 0:
            for i in range(0, len(self.sequence[0, :])):
                self.dna_to_aa(i)
        else:
            # a deletion occured, so the last two nucleotides are removed
            for i in range(0, len(self.sequence[0, :]) - 2):
                self.dna_to_aa(i)

    def dna_to_aa(self, index):
        """
        This method writes the DNA sequence to a amino acid sequence
        :param index: The index of the DNA that needs to be written to a amino acid
        :return:
        """
        # check if the index does not need to be a gap
        if index not in self.gaps:
            self.aa_seq += self.nuc_to_aa[self.nuc_seq[self.pos:self.pos+3]]
            self.pos += 3
        else:
            self.aa_seq += "-"

    def score_matrix(self):
        """
        This method writes a score matrix based on the percentage of alignment with the MSA
        :return: message, perc
        """
        index_msa = []
        message = ""
        perc = 0.0
        for i in self.sequence:
            if i.seq[int(self.pos / 3)] == self.aa_seq[int(self.pos / 3)]:
                message = "Score = 1, the sequence has 100% alignment with the MSA"
        for seq in self.msa:
            index_msa.append(seq.seq[int(self.pos/3)])
        if self.aa_seq[int(self.pos/3)] in index_msa:
            count_aa = index_msa.count(self.aa_seq[int(self.pos/3)])
            total_aa = len(index_msa)
            perc = 100/total_aa*count_aa
            if perc < 12.5:
                message = "Score = 9, the sequence has less than 12.5% alignment with the MSA"
            elif perc < 25:
                message = "Score = 8, the sequence has between 12.5% and 25% alignment with the MSA"
            elif perc < 37.5:
                message = "Score = 7, the sequence has between 25% and 37.5% alignment with the MSA"
            elif perc < 50:
                message = "Score = 6, the sequence has between 37.5% and 50% alignment with the MSA"
            elif perc < 62.5:
                message = "Score = 5, the sequence has between 50% and 62.5% alignment with the MSA"
            elif perc < 75:
                message = "Score = 4, the sequence has between 62.5% and 75% alignment with the MSA"
            elif perc < 87.5:
                message = "Score = 3, the sequence has between 75% and 87.5% alignment with the MSA"
            elif perc < 100:
                message = "Score = 2, the sequence has between 87.5% and 100% alignment with the MSA"
        elif not self.aa_seq[int(self.pos/3)] in index_msa:
            message = "Score = 10, the sequence has no alignment with the MSA"

        return message, perc


def main(args):
    parser = argparse.ArgumentParser()

    parser.add_argument("-n", choices={"A", "C", "G", "T", "-"}, help="The Single Nucleotide Polymorphisme (SNP)")
    parser.add_argument("-p", type=int, help="The position of the nucleotide sequence where the SNP be")
    parser.add_argument("-m", help="Name of the MSA file in fasta format")
    parser.add_argument("genebank_file_gen",
                        help="Name of a file which contains a sequence to compare with the msa in fasta format")

    args = parser.parse_args()

    snp = args.n
    pos = args.p
    msa = args.m
    sequence = args.genebank_file_gen

    result = SNP(pos, snp, sequence, msa)
    result.index_alignment()
    result.add_snp(pos, snp)
    result.check_for_deletion()
    score = result.score_matrix()
    print(score[0])
    print("Alignment percentage = ", str(score[1]) + "%")


if __name__ == '__main__':
    sys.exit(main(sys.argv))