Title: SNP Annotatie
Auther: Marijke Eggink
Date: 5-11-2020

Dit project bevat een python script en twee fasta files.

SNP.py
    python3 SNP.py -n ({G, C, A, T, -}) -p (positon) -m (MSA.fasta file) (genebank.fasta file)

    Dit python script voegt een SNP toe aan een sequentie en vergelijkt deze mutatie verolgens met een MSA file.
    Aan de hand van het alignment percentage krijgt hij dan een score tussen de 1 en 10. Waarbij 10 heel slecht is en
    1 heel goed is.

MSA.fasta
    Dit is een voorbeeld MSA file die gebruikt kan worden bij het runnen van SNP.py

gene.fasta
    Dit is een voorbeeld fasta file van een sequentie die geschrikt is om te runnen van SNP.py